// ==UserScript==
// @name         Filtering liked content
// @namespace    VonFriedricht
// @author       @VonFriedrich
// @copyright    2018+, @VonFriedricht
// @version      1.0.2
// @updateURL    https://gitlab.com/VonFriedrich/twitter-userscript-hide-likes/raw/master/filter-tweets.user.js
// @downloadURL  https://gitlab.com/VonFriedrich/twitter-userscript-hide-likes/raw/master/filter-tweets.user.js
// @description  Hides the Activity Tweets in your timeline. (The stuff others your followers click like on)
// @include      https://twitter.com/*
// @include      http://twitter.com/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    var toFilter = ['ActivityTweet','PyleTweet']; //,'BeforeRecapTweet'
    var filterSheet = document.createElement('style');
    filterSheet.innerHTML = `
        ${toFilter.map(f=>`li[data-suggestion-json*='"suggestion_type":"${f}"']`).join(', ')}{
           display:none !important;
        }
        #page-container{
           padding-bottom:0px !important;
        }
    `;
    document.body.appendChild(filterSheet);
})();