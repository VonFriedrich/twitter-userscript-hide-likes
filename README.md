# Hide-Likes (Twitter Userscript)


Hides the Activity Tweets in your timeline. (The stuff others your followers click like on)

## Installation
1. Browser setup
  - Firefox requires [Greasemonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/) or [Tampermonkey](https://addons.mozilla.org/en-US/firefox/addon/tampermonkey/).
  - Chrome requires [Tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo).
  - Opera requires [Tampermonkey](https://addons.opera.com/en/extensions/details/tampermonkey-beta/).
  
2. Add Userscript via this link: [`https://gitlab.com/VonFriedrich/twitter-userscript-hide-likes/raw/master/filter-tweets.user.js`](https://gitlab.com/VonFriedrich/twitter-userscript-hide-likes/raw/master/filter-tweets.user.js)

3. Refresh Twitter Page.


currently only tested on Firefox v61.0.1 and Tampermonkey v4.7.5788 but I expect it to work on most of the other browsers with an userscript tool, especially for the code being very straightforward.
if there is anything behaving off, tell me about it.

kthxbye